//
//  ArticleTableViewCell.swift
//  Good-News
//
//  Created by Ashutosh Purushottam on 08/12/19.
//  Copyright © 2019 Ashutosh Purushottam. All rights reserved.
//

import UIKit

class ArticleTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
}
