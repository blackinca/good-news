//
//  NewsResponse.swift
//  Good-News
//
//  Created by Ashutosh Purushottam on 07/12/19.
//  Copyright © 2019 Ashutosh Purushottam. All rights reserved.
//

import Foundation

struct ArticleList: Decodable {
    let articles: [Article]
}

struct Article: Decodable {
    let title: String
    let description: String?
}
