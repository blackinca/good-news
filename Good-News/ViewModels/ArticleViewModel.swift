//
//  ArticleViewModel.swift
//  Good-News
//
//  Created by Ashutosh Purushottam on 08/12/19.
//  Copyright © 2019 Ashutosh Purushottam. All rights reserved.
//

import Foundation

// MARK: - ArticleListViewModel
// parent ViewModel for the whole screen
struct ArticleListViewModel {
    
    private let articles: [Article]
    
    init(_ articles: [Article]) {
        self.articles = articles
    }
    
    // Methods for populating tables
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return self.articles.count
    }
    
    func articleAtIndex(_ index: Int) -> ArticleViewModel {
        let article = self.articles[index]
        return ArticleViewModel(article)
    }
}

// MARK: - ArticleViewModel
// ViewModel for the individual article
struct ArticleViewModel {

    private let article: Article
    
    init(_ article: Article) {
        self.article = article
    }
    
    var title: String {
        return self.article.title
    }
    
    var description: String? {
        return self.article.description
    }
}
