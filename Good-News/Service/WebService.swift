//
//  WebService.swift
//  Good-News
//
//  Created by Ashutosh Purushottam on 07/12/19.
//  Copyright © 2019 Ashutosh Purushottam. All rights reserved.
//

import Foundation

class WebService {
    
    
    func getArticles(url: URL, completion: @escaping ([Article]?) -> ()) {

        URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
                completion(nil)
            } else if let data = data {
                let articleList = try? JSONDecoder().decode(ArticleList.self, from: data)
                if let articles = articleList?.articles {
                    completion(articles)
                } else {
                    print("No articles obtained")
                    completion(nil)
                }
            }
        }.resume()
    }

}
