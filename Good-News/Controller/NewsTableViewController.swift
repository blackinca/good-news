//
//  NewsTableViewController.swift
//  Good-News
//
//  Created by Ashutosh Purushottam on 07/12/19.
//  Copyright © 2019 Ashutosh Purushottam. All rights reserved.
//

import UIKit

class NewsTableViewController: UITableViewController {
    
    // MARK: - Properties
    private var articleListViewModel: ArticleListViewModel!
    
    // MARK: - Lifecycle methods

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
        fetchArticles()
    }
    
    // MARK: - Api call

    private func fetchArticles() {
        let url = URL(string: "https://newsapi.org/v2/top-headlines?country=us&apiKey=d887522a112941729210989425b990c3")!
        
        WebService().getArticles(url: url) { articles in
            if let articles = articles {
                //print(articles)
                self.articleListViewModel = ArticleListViewModel(articles)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
            
        }
    }
    
    // MARK: - TableView methods

    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.articleListViewModel != nil ? self.articleListViewModel.numberOfSections() : 0
    }
        
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.articleListViewModel != nil ? self.articleListViewModel.numberOfRowsInSection(section) : 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleCell", for: indexPath) as! ArticleTableViewCell
        let articleViewModel = self.articleListViewModel.articleAtIndex(indexPath.row)
        cell.titleLabel.text = articleViewModel.title
        cell.descriptionLabel.text = articleViewModel.description ?? "No description"
        return cell
    }
    
}
